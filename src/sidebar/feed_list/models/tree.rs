use std::collections::HashSet;

use super::category::FeedListCategoryModel;
use super::error::{FeedListModelError, FeedListModelErrorKind};
use super::feed::FeedListFeedModel;
use super::item::FeedListItem;
use super::FeedListItemID;
use crate::sidebar::SidebarIterateItem;
use news_flash::models::{Category, CategoryID, Feed, FeedID, FeedMapping, NEWSFLASH_TOPLEVEL};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct FeedListTree {
    top_level_id: CategoryID,
    pub top_level: Vec<FeedListItem>,
}

impl FeedListTree {
    pub fn new() -> Self {
        FeedListTree {
            top_level_id: NEWSFLASH_TOPLEVEL.clone(),
            top_level: Vec::new(),
        }
    }

    pub fn add_feed(&mut self, feed: &Feed, mapping: &FeedMapping, item_count: i64) -> Result<(), FeedListModelError> {
        if mapping.category_id == self.top_level_id {
            let contains_feed = self.top_level.iter().any(|item| {
                if let FeedListItem::Feed(item) = item {
                    return item.id == feed.feed_id;
                }
                false
            });
            if !contains_feed {
                let feed = FeedListFeedModel::new(feed, mapping, item_count, 0);
                let item = FeedListItem::Feed(feed);
                self.top_level.push(item);
                self.top_level.sort();
            } else {
                return Err(FeedListModelErrorKind::AddDuplicateFeed.into());
            }
            return Ok(());
        }

        if let Some(parent) = self.find_category(&mapping.category_id) {
            let feed = FeedListFeedModel::new(feed, mapping, item_count, parent.level + 1);
            let item = FeedListItem::Feed(feed);
            parent.add_child(item);
            return Ok(());
        }

        Err(FeedListModelErrorKind::AddFeedNoParent.into())
    }

    pub fn add_category(&mut self, category: &Category, item_count: i64) -> Result<(), FeedListModelError> {
        if category.parent_id == self.top_level_id {
            let contains_category = self.top_level.iter().any(|item| {
                if let FeedListItem::Category(item) = item {
                    return item.id == category.category_id;
                }
                false
            });
            if !contains_category {
                let category_ = FeedListCategoryModel::new(&category, item_count, 0);
                let item = FeedListItem::Category(category_);

                self.top_level.push(item);
                self.top_level.sort();
            } else {
                return Err(FeedListModelErrorKind::AddDuplicateCategory.into());
            }
            return Ok(());
        }

        if let Some(parent) = self.find_category(&category.parent_id) {
            let category_ = FeedListCategoryModel::new(&category, item_count, parent.level + 1);
            let item = FeedListItem::Category(category_);
            parent.add_child(item);
            return Ok(());
        }

        Err(FeedListModelErrorKind::AddCategoryNoParent.into())
    }

    fn find_category(&mut self, id: &CategoryID) -> Option<&mut FeedListCategoryModel> {
        Self::search_subcategories_for_category(id, &mut self.top_level)
    }

    fn search_subcategories_for_category<'a>(
        id: &CategoryID,
        children: &'a mut Vec<FeedListItem>,
    ) -> Option<&'a mut FeedListCategoryModel> {
        for item in children {
            if let FeedListItem::Category(category_model) = item {
                if &category_model.id == id {
                    return Some(category_model);
                } else if !category_model.children.is_empty() {
                    if let Some(category_model) =
                        Self::search_subcategories_for_category(id, &mut category_model.children)
                    {
                        return Some(category_model);
                    }
                }
            }
        }
        None
    }

    pub fn get_expanded_categories(&mut self) -> HashSet<CategoryID> {
        let mut expanded = HashSet::new();
        Self::get_expanded_categories_recurse(&mut self.top_level, &mut expanded);
        expanded
    }

    fn get_expanded_categories_recurse(children: &'_ mut Vec<FeedListItem>, expanded: &mut HashSet<CategoryID>) {
        for item in children {
            if let FeedListItem::Category(category_model) = item {
                if category_model.expanded {
                    expanded.insert(category_model.id.clone());
                }
                Self::get_expanded_categories_recurse(&mut category_model.children, expanded);
            }
        }
    }

    pub fn apply_expanded_categories(&mut self, expanded: &HashSet<CategoryID>) {
        Self::apply_expanded_categories_recurse(&mut self.top_level, expanded);
    }

    fn apply_expanded_categories_recurse(children: &'_ mut Vec<FeedListItem>, expanded: &HashSet<CategoryID>) {
        for item in children {
            if let FeedListItem::Category(category_model) = item {
                category_model.expanded = expanded.contains(&category_model.id);
                Self::apply_expanded_categories_recurse(&mut category_model.children, expanded);
            }
        }
    }

    pub fn collapse_expand_category(
        &mut self,
        category_id: &CategoryID,
    ) -> Option<(Vec<(FeedID, CategoryID)>, Vec<CategoryID>, bool)> {
        if let Some(category) = self.find_category(category_id) {
            let expanded = category.expand_collapse();
            let (feed_ids, category_ids) = Self::category_child_ids(&category);
            return Some((feed_ids, category_ids, expanded));
        }
        None
    }

    fn category_child_ids(category: &FeedListCategoryModel) -> (Vec<(FeedID, CategoryID)>, Vec<CategoryID>) {
        let mut feed_ids: Vec<(FeedID, CategoryID)> = Vec::new();
        let mut category_ids: Vec<CategoryID> = Vec::new();
        for item in &category.children {
            match item {
                FeedListItem::Feed(feed) => feed_ids.push((feed.id.clone(), feed.parent_id.clone())),
                FeedListItem::Category(category) => {
                    category_ids.push(category.id.clone());
                    if category.expanded {
                        let (mut sub_feeds, mut sub_categories) = Self::category_child_ids(&category);
                        feed_ids.append(&mut sub_feeds);
                        category_ids.append(&mut sub_categories);
                    }
                }
            }
        }
        (feed_ids, category_ids)
    }

    pub fn calculate_dnd(&self, pos: i32) -> Result<(CategoryID, i32), FeedListModelError> {
        let mut pos_iter = 0;
        self.calc_dnd_subcategory(&self.top_level, &self.top_level_id, pos, &mut pos_iter)
    }

    fn calc_dnd_subcategory(
        &self,
        category: &[FeedListItem],
        parent_id: &CategoryID,
        list_pos: i32,
        global_pos_iter: &mut i32,
    ) -> Result<(CategoryID, i32), FeedListModelError> {
        if global_pos_iter == &list_pos {
            return Ok((parent_id.clone(), 0));
        }

        for (local_pos, item) in category.iter().enumerate() {
            *global_pos_iter += 1;

            if let FeedListItem::Category(model) = item {
                if let Ok((parent, pos)) =
                    self.calc_dnd_subcategory(&model.children, &model.id, list_pos, global_pos_iter)
                {
                    return Ok((parent, pos));
                }
            }

            if global_pos_iter == &list_pos {
                let parent = match item {
                    FeedListItem::Category(model) => model.parent_id.clone(),
                    FeedListItem::Feed(model) => model.parent_id.clone(),
                };
                return Ok((parent, local_pos as i32));
            }
        }
        Err(FeedListModelErrorKind::DnD.into())
    }

    pub fn calculate_selection(&self, selected_id: &FeedListItemID) -> Option<(FeedListItemID, String, i64)> {
        Self::calculate_selection_internal(selected_id, &self.top_level)
    }

    fn calculate_selection_internal(
        selected_id: &FeedListItemID,
        items: &[FeedListItem],
    ) -> Option<(FeedListItemID, String, i64)> {
        for item in items {
            match item {
                FeedListItem::Feed(feed) => {
                    if let FeedListItemID::Feed(selected_feed_id, selected_feed_parent_id) = selected_id {
                        if &feed.id == selected_feed_id && &feed.parent_id == selected_feed_parent_id {
                            return Some((
                                FeedListItemID::Feed(feed.id.clone(), feed.parent_id.clone()),
                                feed.label.clone(),
                                feed.item_count,
                            ));
                        }
                    }
                }
                FeedListItem::Category(category) => {
                    if let FeedListItemID::Category(selected_category_id) = selected_id {
                        if &category.id == selected_category_id {
                            return Some((
                                FeedListItemID::Category(category.id.clone()),
                                category.label.clone(),
                                category.item_count,
                            ));
                        }
                    }
                    if let Some(selection) = Self::calculate_selection_internal(selected_id, &category.children) {
                        return Some(selection);
                    }
                }
            }
        }
        None
    }

    pub fn calculate_next_item(
        &mut self,
        selected_id: &FeedListItemID,
        only_show_relevant: bool,
    ) -> SidebarIterateItem {
        let mut selected_found = false;
        self.top_level.sort();
        Self::iterate_next_internal(selected_id, &self.top_level, &mut selected_found, only_show_relevant)
    }

    fn iterate_next_internal(
        selected_id: &FeedListItemID,
        items: &[FeedListItem],
        selected_found: &mut bool,
        only_show_relevant: bool,
    ) -> SidebarIterateItem {
        for item in items {
            match item {
                FeedListItem::Feed(feed) => {
                    if !*selected_found {
                        if let FeedListItemID::Feed(id, parent) = selected_id {
                            if id == &feed.id && parent == &feed.parent_id {
                                *selected_found = true;
                                continue;
                            }
                        }
                    } else if !only_show_relevant || feed.item_count > 0 {
                        return SidebarIterateItem::SelectFeedListFeed(feed.id.clone(), feed.parent_id.clone());
                    }
                }
                FeedListItem::Category(category) => {
                    if !*selected_found {
                        if let FeedListItemID::Category(id) = selected_id {
                            if id == &category.id {
                                *selected_found = true;
                                if !category.expanded {
                                    continue;
                                }
                            }
                        }
                    } else {
                        return SidebarIterateItem::SelectFeedListCategory(category.id.clone());
                    }

                    match Self::iterate_next_internal(
                        selected_id,
                        &category.children,
                        selected_found,
                        only_show_relevant,
                    ) {
                        SidebarIterateItem::SelectFeedListCategory(category) => {
                            return SidebarIterateItem::SelectFeedListCategory(category)
                        }
                        SidebarIterateItem::SelectFeedListFeed(feed, parent_id) => {
                            return SidebarIterateItem::SelectFeedListFeed(feed, parent_id)
                        }
                        _ => {}
                    }
                }
            }
        }
        SidebarIterateItem::TagListSelectFirstItem
    }

    pub fn calculate_prev_item(
        &mut self,
        selected_id: &FeedListItemID,
        only_show_relevant: bool,
    ) -> SidebarIterateItem {
        let mut selected_found = false;
        self.top_level.sort();
        Self::iterate_prev_internal(selected_id, &self.top_level, &mut selected_found, only_show_relevant)
    }

    fn iterate_prev_internal(
        selected_id: &FeedListItemID,
        items: &[FeedListItem],
        selected_found: &mut bool,
        only_show_relevant: bool,
    ) -> SidebarIterateItem {
        for item in items.iter().rev() {
            match item {
                FeedListItem::Feed(feed) => {
                    if !*selected_found {
                        if let FeedListItemID::Feed(id, parent) = selected_id {
                            if id == &feed.id && parent == &feed.parent_id {
                                *selected_found = true;
                                continue;
                            }
                        }
                    } else if !only_show_relevant || feed.item_count > 0 {
                        return SidebarIterateItem::SelectFeedListFeed(feed.id.clone(), feed.parent_id.clone());
                    }
                }
                FeedListItem::Category(category) => {
                    if category.expanded {
                        match Self::iterate_prev_internal(
                            selected_id,
                            &category.children,
                            selected_found,
                            only_show_relevant,
                        ) {
                            SidebarIterateItem::SelectFeedListCategory(category) => {
                                return SidebarIterateItem::SelectFeedListCategory(category)
                            }
                            SidebarIterateItem::SelectFeedListFeed(feed, parent_id) => {
                                return SidebarIterateItem::SelectFeedListFeed(feed, parent_id)
                            }
                            _ => {}
                        }
                    }

                    if !*selected_found {
                        if let FeedListItemID::Category(id) = selected_id {
                            if id == &category.id {
                                *selected_found = true;
                                if !category.expanded {
                                    continue;
                                }
                            }
                        }
                    } else {
                        return SidebarIterateItem::SelectFeedListCategory(category.id.clone());
                    }
                }
            }
        }
        SidebarIterateItem::SelectAll
    }

    pub fn get_first(&mut self, only_show_relevant: bool) -> Option<FeedListItemID> {
        self.top_level.sort();

        for item in &self.top_level {
            match item {
                FeedListItem::Feed(feed) => {
                    if only_show_relevant && feed.item_count > 0 {
                        return Some(FeedListItemID::Feed(feed.id.clone(), feed.parent_id.clone()));
                    }
                }
                FeedListItem::Category(category) => {
                    return Some(FeedListItemID::Category(category.id.clone()));
                }
            }
        }
        None
    }

    pub fn get_last(&mut self, only_show_relevant: bool) -> Option<FeedListItemID> {
        self.top_level.sort();
        Self::get_last_internal(&self.top_level, only_show_relevant)
    }

    fn get_last_internal(items: &[FeedListItem], only_show_relevant: bool) -> Option<FeedListItemID> {
        for item in items.iter().rev() {
            match item {
                FeedListItem::Feed(feed) => {
                    if only_show_relevant && feed.item_count > 0 {
                        return Some(FeedListItemID::Feed(feed.id.clone(), feed.parent_id.clone()));
                    }
                }
                FeedListItem::Category(category) => {
                    if category.expanded {
                        match Self::get_last_internal(&category.children, only_show_relevant) {
                            Some(last_item) => return Some(last_item),
                            None => return Some(FeedListItemID::Category(category.id.clone())),
                        }
                    }

                    return Some(FeedListItemID::Category(category.id.clone()));
                }
            }
        }
        None
    }

    #[allow(dead_code)]
    pub fn print(&self) {
        self.print_internal(&self.top_level, &mut 0);
    }

    fn print_internal(&self, category: &[FeedListItem], level: &mut i32) {
        let mut new_level = *level + 1;
        for item in category {
            for _ in 0..new_level {
                print!("-- ");
            }

            match item {
                FeedListItem::Category(model) => {
                    println!("{}", model);
                    self.print_internal(&model.children, &mut new_level);
                }
                FeedListItem::Feed(model) => {
                    println!("{}", model);
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::sidebar::feed_list::models::FeedListTree;
    use news_flash::models::{Category, CategoryID, CategoryType, Feed, FeedID, FeedMapping, NEWSFLASH_TOPLEVEL};

    fn building_blocks() -> (
        Category,
        Category,
        Category,
        Category,
        Feed,
        Feed,
        FeedMapping,
        FeedMapping,
        FeedMapping,
        FeedMapping,
    ) {
        let category_1 = Category {
            category_id: CategoryID::new("category_1"),
            label: "Category 1".to_owned(),
            parent_id: NEWSFLASH_TOPLEVEL.clone(),
            sort_index: Some(0),
            category_type: CategoryType::Default,
        };
        let category_2 = Category {
            category_id: CategoryID::new("category_2"),
            label: "Category 2".to_owned(),
            parent_id: NEWSFLASH_TOPLEVEL.clone(),
            sort_index: Some(1),
            category_type: CategoryType::Default,
        };
        let category_3 = Category {
            category_id: CategoryID::new("category_3"),
            label: "Category 3".to_owned(),
            parent_id: NEWSFLASH_TOPLEVEL.clone(),
            sort_index: Some(2),
            category_type: CategoryType::Default,
        };
        let category_4 = Category {
            category_id: CategoryID::new("category_4"),
            label: "Category 4".to_owned(),
            parent_id: CategoryID::new("category_2"),
            sort_index: Some(0),
            category_type: CategoryType::Default,
        };
        let feed_1 = Feed {
            feed_id: FeedID::new("feed_1"),
            label: "Feed 1".to_owned(),
            feed_url: None,
            icon_url: None,
            sort_index: Some(0),
            website: None,
        };
        let feed_2 = Feed {
            feed_id: FeedID::new("feed_2"),
            label: "Feed 2".to_owned(),
            feed_url: None,
            icon_url: None,
            sort_index: Some(1),
            website: None,
        };
        let mapping_1 = FeedMapping {
            feed_id: FeedID::new("feed_1"),
            category_id: CategoryID::new("category_2"),
        };
        let mapping_2 = FeedMapping {
            feed_id: FeedID::new("feed_1"),
            category_id: CategoryID::new("category_1"),
        };
        let mapping_3 = FeedMapping {
            feed_id: FeedID::new("feed_2"),
            category_id: CategoryID::new("category_3"),
        };
        let mapping_4 = FeedMapping {
            feed_id: FeedID::new("feed_2"),
            category_id: CategoryID::new("category_4"),
        };

        (
            category_1, category_2, category_3, category_4, feed_1, feed_2, mapping_1, mapping_2, mapping_3, mapping_4,
        )
    }

    #[test]
    fn calc_dnd_1() {
        let (category_1, category_2, category_3, _, feed_1, feed_2, mapping_1, _, mapping_3, _) = building_blocks();

        let mut tree = FeedListTree::new();
        tree.add_category(&category_1, 5).expect("Failed to add category_1");
        tree.add_category(&category_2, 0).expect("Failed to add category_2");
        tree.add_category(&category_3, 0).expect("Failed to add category_3");
        tree.add_feed(&feed_1, &mapping_1, 1)
            .expect("Failed to add feed_1 with mapping_1");
        tree.add_feed(&feed_2, &mapping_3, 1)
            .expect("Failed to add feed_2 with mapping_3");

        let (id, pos) = tree.calculate_dnd(2).expect("Failed to calculate drag&drop result");

        assert_eq!(id, CategoryID::new("category_2"));
        assert_eq!(pos, 0);
    }
}
